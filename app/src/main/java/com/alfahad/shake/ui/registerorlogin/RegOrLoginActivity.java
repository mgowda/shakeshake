package com.alfahad.shake.ui.registerorlogin;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivityRegOrLoginBinding;
import com.alfahad.shake.ui.login.LoginActivity;
import com.alfahad.shake.ui.regdetail.RegDetailActivity;

public class RegOrLoginActivity extends Activity {

    ActivityRegOrLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_reg_or_login);
        binding.setHandlers(this);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_rorl_reg:
                Intent regintent = new Intent(RegOrLoginActivity.this, RegDetailActivity.class);
                startActivity(regintent);
                break;
            case R.id.btn_rorl_login:
                Intent loginintent = new Intent(RegOrLoginActivity.this, LoginActivity.class);
                startActivity(loginintent);
                break;
        }
    }
}
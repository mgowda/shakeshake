package com.alfahad.shake.ui.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.alfahad.shake.R;
import com.alfahad.shake.ui.registerorlogin.RegOrLoginActivity;

import static com.alfahad.shake.utils.AppConstants.DELAY;

public class SplashActivity extends Activity {
    Handler handler;
    Runnable runnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startTimer();
    }

    private void startTimer() {
        handler=new Handler();
        runnable= new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this,RegOrLoginActivity.class);
                startActivity(intent);
                finish();
            }
        };
        handler.postDelayed(runnable,DELAY);
    }
}

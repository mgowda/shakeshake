package com.alfahad.shake.ui.regdetail;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivityRegDetailBinding;
import com.alfahad.shake.ui.signup.SignUpActivity;

public class RegDetailActivity extends Activity {

    ActivityRegDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_reg_detail);
        binding.setHandlers(this);
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_rd_register:
                final Dialog dialog = new Dialog(RegDetailActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.verificationdailog);
                Button dialogButton = (Button) dialog.findViewById(R.id.btn_reg_vfn);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        final Dialog dialog1 = new Dialog(RegDetailActivity.this);
                        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog1.setContentView(R.layout.verificationdonedailog);
                        Button dialogButton1 = (Button) dialog1.findViewById(R.id.btn_vfn_done);
                        dialogButton1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog1.dismiss();
                                Intent i = new Intent(RegDetailActivity.this, SignUpActivity.class);
                                startActivity(i);
                            }
                        });

                        dialog1.show();
                    }
                });
                dialog.show();
                break;

            case R.id.btn_reg_vfn:

                break;
        }


    }

}


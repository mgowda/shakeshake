package com.alfahad.shake.ui.signupsucess;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivitySignUpSucessBinding;

public class SignUpSucessActivity extends Activity {

    ActivitySignUpSucessBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up_sucess);
        binding.setHandlers(this);
    }
}

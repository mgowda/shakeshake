package com.alfahad.shake.ui.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.alfahad.shake.R;
import com.alfahad.shake.ShakeShakeApplication;
import com.alfahad.shake.databinding.ActivityDashboardBinding;
import com.alfahad.shake.ui.db_pts_used.Db_Pts_UsedActivity;

import javax.inject.Inject;

public class DashboardActivity extends Activity implements DashboardContract.View{
    @Inject
    DashboardPresenter presenter;
    ActivityDashboardBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_dashboard);
        DaggerDashboardComponent.builder().appComponent(ShakeShakeApplication.get()).dashboardModule(new DashboardModule(this)).build();
        binding.setHandlers(this);
    }

    public void onClick(View view)
    {
        switch (view.getId()){
            case R.id.ll_db_p_used:
                binding.llDbPUsed.setBackgroundResource(R.drawable.bg_red);
                binding.tvDbPUsed.setTextColor(Color.WHITE);
                binding.tvDbPUsedVal.setTextColor(Color.WHITE);

                binding.llDbPRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPRmg.setTextColor(Color.GRAY);
                binding.tvDbPRmgVal.setTextColor(Color.RED);

                binding.llDbGWon.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGWon.setTextColor(Color.GRAY);
                binding.tvDbGWonVal.setTextColor(Color.RED);

                binding.llDbGPrg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGPrg.setTextColor(Color.GRAY);
                binding.tvDbGPrgVal.setTextColor(Color.RED);

                binding.llDbSUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSUsed.setTextColor(Color.GRAY);
                binding.tvDbSUsedVal.setTextColor(Color.RED);

                binding.llDbSRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSRmg.setTextColor(Color.GRAY);
                binding.tvDbSRmgVal.setTextColor(Color.RED);

                Intent intent = new Intent(DashboardActivity.this, Db_Pts_UsedActivity.class);
                startActivity(intent);

                break;

            case R.id.ll_db_p_rmg:
                binding.llDbPUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPUsed.setTextColor(Color.GRAY);
                binding.tvDbPUsedVal.setTextColor(Color.RED);

                binding.llDbPRmg.setBackgroundResource(R.drawable.bg_red);
                binding.tvDbPRmg.setTextColor(Color.WHITE);
                binding.tvDbPRmgVal.setTextColor(Color.WHITE);

                binding.llDbGWon.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGWon.setTextColor(Color.GRAY);
                binding.tvDbGWonVal.setTextColor(Color.RED);

                binding.llDbGPrg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGPrg.setTextColor(Color.GRAY);
                binding.tvDbGPrgVal.setTextColor(Color.RED);

                binding.llDbSUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSUsed.setTextColor(Color.GRAY);
                binding.tvDbSUsedVal.setTextColor(Color.RED);

                binding.llDbSRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSRmg.setTextColor(Color.GRAY);
                binding.tvDbSRmgVal.setTextColor(Color.RED);
                break;

            case R.id.ll_db_g_won:
                binding.llDbPUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPUsed.setTextColor(Color.GRAY);
                binding.tvDbPUsedVal.setTextColor(Color.RED);

                binding.llDbPRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPRmg.setTextColor(Color.GRAY);
                binding.tvDbPRmgVal.setTextColor(Color.RED);

                binding.llDbGWon.setBackgroundResource(R.drawable.bg_red);
                binding.tvDbGWon.setTextColor(Color.WHITE);
                binding.tvDbGWonVal.setTextColor(Color.WHITE);

                binding.llDbGPrg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGPrg.setTextColor(Color.GRAY);
                binding.tvDbGPrgVal.setTextColor(Color.RED);

                binding.llDbSUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSUsed.setTextColor(Color.GRAY);
                binding.tvDbSUsedVal.setTextColor(Color.RED);

                binding.llDbSRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSRmg.setTextColor(Color.GRAY);
                binding.tvDbSRmgVal.setTextColor(Color.RED);
                break;

            case R.id.ll_db_g_prg:
                binding.llDbPUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPUsed.setTextColor(Color.GRAY);
                binding.tvDbPUsedVal.setTextColor(Color.RED);

                binding.llDbPRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPRmg.setTextColor(Color.GRAY);
                binding.tvDbPRmgVal.setTextColor(Color.RED);

                binding.llDbGWon.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGWon.setTextColor(Color.GRAY);
                binding.tvDbGWonVal.setTextColor(Color.RED);

                binding.llDbGPrg.setBackgroundResource(R.drawable.bg_red);
                binding.tvDbGPrg.setTextColor(Color.WHITE);
                binding.tvDbGPrgVal.setTextColor(Color.WHITE);

                binding.llDbSUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSUsed.setTextColor(Color.GRAY);
                binding.tvDbSUsedVal.setTextColor(Color.RED);

                binding.llDbSRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSRmg.setTextColor(Color.GRAY);
                binding.tvDbSRmgVal.setTextColor(Color.RED);
                break;

            case R.id.ll_db_s_used:
                binding.llDbPUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPUsed.setTextColor(Color.GRAY);
                binding.tvDbPUsedVal.setTextColor(Color.RED);

                binding.llDbPRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPRmg.setTextColor(Color.GRAY);
                binding.tvDbPRmgVal.setTextColor(Color.RED);

                binding.llDbGWon.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGWon.setTextColor(Color.GRAY);
                binding.tvDbGWonVal.setTextColor(Color.RED);

                binding.llDbGPrg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGPrg.setTextColor(Color.GRAY);
                binding.tvDbGPrgVal.setTextColor(Color.RED);

                binding.llDbSUsed.setBackgroundResource(R.drawable.bg_red);
                binding.tvDbSUsed.setTextColor(Color.WHITE);
                binding.tvDbSUsedVal.setTextColor(Color.WHITE);

                binding.llDbSRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSRmg.setTextColor(Color.GRAY);
                binding.tvDbSRmgVal.setTextColor(Color.RED);
                break;

            case R.id.ll_db_s_rmg:
                binding.llDbPUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPUsed.setTextColor(Color.GRAY);
                binding.tvDbPUsedVal.setTextColor(Color.RED);

                binding.llDbPRmg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbPRmg.setTextColor(Color.GRAY);
                binding.tvDbPRmgVal.setTextColor(Color.RED);

                binding.llDbGWon.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGWon.setTextColor(Color.GRAY);
                binding.tvDbGWonVal.setTextColor(Color.RED);

                binding.llDbGPrg.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbGPrg.setTextColor(Color.GRAY);
                binding.tvDbGPrgVal.setTextColor(Color.RED);

                binding.llDbSUsed.setBackgroundResource(R.drawable.border_gray);
                binding.tvDbSUsed.setTextColor(Color.GRAY);
                binding.tvDbSUsedVal.setTextColor(Color.RED);

                binding.llDbSRmg.setBackgroundResource(R.drawable.bg_red);
                binding.tvDbSRmg.setTextColor(Color.WHITE);
                binding.tvDbSRmgVal.setTextColor(Color.WHITE);
                break;
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onUnknownError(String message, String error) {

    }

    @Override
    public void onTimeout() {

    }

    @Override
    public void onNetworkError() {

    }

    @Override
    public void onFailure(String errorMsg) {

    }

    @Override
    public void onConnectionError() {

    }
}

package com.alfahad.shake.ui.resetpwd;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivityResetPwdBinding;
import com.alfahad.shake.ui.pwdsucess.PwdResetSucessActivity;

public class ResetPwdActivity extends Activity {

    ActivityResetPwdBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_reset_pwd);
        binding.setHandlers(this);
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_resetpwd:
                Intent i = new Intent(ResetPwdActivity.this, PwdResetSucessActivity.class);
                startActivity(i);
                break;
        }
    }
}

package com.alfahad.shake.ui.signup;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivitySignUpBinding;
import com.alfahad.shake.ui.signupsucess.SignUpSucessActivity;

public class SignUpActivity extends Activity {

    ActivitySignUpBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up);
        binding.setHandlers(this);
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_sud_signup:
                Intent intent = new Intent(SignUpActivity.this, SignUpSucessActivity.class);
                startActivity(intent);
                break;
        }
    }
}
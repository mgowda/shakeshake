package com.alfahad.shake.ui.otp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivityOtpBinding;
import com.alfahad.shake.ui.resetpwd.ResetPwdActivity;

public class OtpActivity extends Activity {

    ActivityOtpBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_otp);
        binding.setHandlers(this);
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tv_resendotp:

                break;
            case R.id.btn_otp_vfy:
                final Dialog dialog = new Dialog(OtpActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.otpvfndailog);
                Button dialogButton = (Button) dialog.findViewById(R.id.btn_otpvfn_ok);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent = new Intent(OtpActivity.this, ResetPwdActivity.class);
                        startActivity(intent);
                    }
                });
                dialog.show();
                break;
        }
    }
}


package com.alfahad.shake.ui.pwdsucess;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.alfahad.shake.R;
import com.alfahad.shake.databinding.ActivityPwdResetSucessBinding;
import com.alfahad.shake.ui.login.LoginActivity;

public class PwdResetSucessActivity extends Activity {

    ActivityPwdResetSucessBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pwd_reset_sucess);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_pwd_reset_sucess);
        binding.setHandlers(this);
    }
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_prs_ok:
                Intent i = new Intent(PwdResetSucessActivity.this, LoginActivity.class);
                startActivity(i);
                break;
        }
    }
}

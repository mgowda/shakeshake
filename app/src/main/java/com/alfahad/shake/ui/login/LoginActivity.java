package com.alfahad.shake.ui.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.alfahad.shake.R;


import com.alfahad.shake.databinding.ActivityLoginBinding;
import com.alfahad.shake.ui.dashboard.DashboardActivity;
import com.alfahad.shake.ui.otp.OtpActivity;

public class LoginActivity extends Activity {

    ActivityLoginBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        binding.setHandlers(LoginActivity.this);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_login:
                Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                startActivity(i);
                break;

            case R.id.tv_frgtpwd:
                final Dialog dialog = new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.frgtpwddailog);
                Button dialogButton = (Button) dialog.findViewById(R.id.btn_fp_mobnum);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                        startActivity(intent);
                    }
                });
                dialog.show();
                break;
        }
    }

}


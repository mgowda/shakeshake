package com.alfahad.shake.utils;

/**
 * Created by user on 25-05-2018.
 */

public class AppConstants {
    public static final int DELAY=4000;
    public static final long READ_TIMEOUT=1;
    public static final long CONNECT_TIMEOUT=1;
    public static final String BASE_URL="http://api.shakeshake.com/";
}

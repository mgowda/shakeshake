package com.alfahad.shake.utils;

import android.annotation.SuppressLint;
import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.alfahad.shake.R;

/**
 * Created by user on 29-05-2018.
 */

public class DataBindingAdapter {

    @BindingAdapter({"bind:typeface"})
    public static void loadTypeface(TextView textView, String fonts) {
        Typeface typeface= Typeface.createFromAsset(textView.getContext().getAssets(),"fonts/" +fonts);
        textView.setTypeface(typeface);
    }
/*
    @BindingAdapter("touchListener")
    public static void setTouchListener(View self,boolean value){
        self.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // Check if the button is PRESSED
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                  view.setBackgroundColor(Color.RED);
                }// Check if the button is RELEASED
                 if (event.getAction() == MotionEvent.ACTION_UP) {
                  view.setBackgroundColor(Color.WHITE);
                }
                return true;
            }
        });
    }
     app:touchListener="@{true}"
    */
}

package com.alfahad.shake.dagger.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by intel on 3/19/2018.
 */

@Module
public class AppModule {

    private final Context application;
    public AppModule(Context application){
        this.application=application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext(){
        return application;
    }

    @Provides
    public SharedPreferences providesSharedPreferences(Context context){
        return  context.getSharedPreferences("myprefs",Context.MODE_PRIVATE);
    }
}

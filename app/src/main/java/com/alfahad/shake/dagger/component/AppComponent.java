package com.alfahad.shake.dagger.component;

import android.content.Context;
import android.content.SharedPreferences;


import com.alfahad.shake.ShakeShakeApplication;
import com.alfahad.shake.dagger.modules.AppModule;
import com.alfahad.shake.dagger.modules.NetModule;
import com.alfahad.shake.net.RestApi;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by intel on 3/19/2018.
 */
@Component(modules = {AppModule.class, NetModule.class})
@Singleton
public interface AppComponent {

    Context context();
    SharedPreferences sharedPreferences();
    RestApi restApi();

    void inject(ShakeShakeApplication application);
}

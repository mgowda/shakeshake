package com.alfahad.shake.dagger.modules;

import android.content.Context;

import com.alfahad.shake.ShakeShakeApplication;
import com.alfahad.shake.net.RestApi;
import com.alfahad.shake.utils.AppConstants;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.readystatesoftware.chuck.ChuckInterceptor;


import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by intel on 3/19/2018.
 */
@Module
public class NetModule {
    private String baseUrl;
    private ShakeShakeApplication application;

   public NetModule(String baseUrl, ShakeShakeApplication application){
     this.baseUrl=baseUrl;
     this.application=application;
   }

    @Provides
    @Singleton
    public OkHttpClient getClient(Context context) {
        // Add the interceptor to OkHttpClient
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new ChuckInterceptor(context));
        builder.readTimeout(AppConstants.READ_TIMEOUT, TimeUnit.MINUTES);
        builder.connectTimeout(AppConstants.CONNECT_TIMEOUT, TimeUnit.MINUTES);
        return builder.build();
    }
    @Provides
    @Singleton
    public RestApi provideRestApi(OkHttpClient okHttpClient ) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build().create(RestApi.class);
    }

}
